# chexsum

A tool for generating and testing the validity of checksum-hex-encoded data for use on Casper networks.

## Installing chexsum

```
cargo install chexsum --git https://gitlab.com/Fraser999/chexsum.git
```

## Running chexsum

```
chexsum --help
```

## License

Licensed under the [Apache License Version 2.0](LICENSE).

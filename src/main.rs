use std::{fs, path::PathBuf, process};

use casper_types::{
    checksummed_hex::{self, SMALL_BYTES_COUNT},
    crypto, AsymmetricType, PublicKey, SecretKey, Tagged,
};
use clap::{crate_description, crate_name, crate_version, value_parser, Arg, ArgMatches, Command};
use rand::{Rng, RngCore};

const GEN_PUB_KEYS: &str = "gen-pub-keys";
const GEN_RANDOM: &str = "gen-random";
const CHECK_PUB_KEYS: &str = "check-pub-keys";
const CHECK_RANDOM: &str = "check-random";
const COUNT: &str = "count";
const FILE: &str = "file";
const HEX_CHARS: [char; 22] = [
    '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f', 'A', 'B', 'C',
    'D', 'E', 'F',
];

/// Takes a slice of bytes and breaks it up into a vector of *nibbles* (ie, 4-bit values)
/// represented as `u8`s.
fn bytes_to_nibbles<'a, T: 'a + AsRef<[u8]>>(input: &'a T) -> impl Iterator<Item = u8> + 'a {
    input
        .as_ref()
        .iter()
        .flat_map(move |byte| [4, 0].iter().map(move |offset| (byte >> offset) & 0x0f))
}

/// Takes a slice of bytes and outputs an infinite cyclic stream of bits for those bytes.
fn bytes_to_bits_cycle(bytes: Vec<u8>) -> impl Iterator<Item = bool> {
    bytes
        .into_iter()
        .cycle()
        .flat_map(move |byte| (0..8usize).map(move |offset| ((byte >> offset) & 0x01) == 0x01))
}

/// Returns the bytes encoded as hexadecimal with mixed-case based checksums following a scheme
/// similar to [EIP-55](https://eips.ethereum.org/EIPS/eip-55).
///
/// Key differences:
///   - Works on any length of data, not just 20-byte addresses
///   - Uses Blake2b hashes rather than Keccak
///   - Uses hash bits rather than nibbles
fn encode_iter<'a, T: 'a + AsRef<[u8]>>(input: &'a T) -> impl Iterator<Item = char> + 'a {
    let nibbles = bytes_to_nibbles(input);
    let mut hash_bits = bytes_to_bits_cycle(crypto::blake2b(input.as_ref()).to_vec());
    nibbles.map(move |mut nibble| {
        // Base 16 numbers greater than 10 are represented by the ascii characters a through f.
        if nibble >= 10 && hash_bits.next().unwrap_or(true) {
            // We are using nibble to index HEX_CHARS, so adding 6 to nibble gives us the index
            // of the uppercase character. HEX_CHARS[10] == 'a', HEX_CHARS[16] == 'A'.
            nibble += 6;
        }
        HEX_CHARS[nibble as usize]
    })
}

fn cli() -> Command {
    Command::new(crate_name!())
        .version(crate_version!())
        .about(crate_description!())
        .subcommand(
            Command::new(GEN_PUB_KEYS)
                .about("generate checksum-hex-encoded random public keys")
                .arg(
                    Arg::new(COUNT)
                        .long(COUNT)
                        .short('c')
                        .default_value("20")
                        .value_name("INTEGER")
                        .value_parser(value_parser!(u8).range(1..))
                        .help("the number of values to generate"),
                ),
        )
        .subcommand(
            Command::new(GEN_RANDOM)
                .about("generate checksum-hex-encoded random data")
                .arg(
                    Arg::new(COUNT)
                        .long(COUNT)
                        .short('c')
                        .default_value("20")
                        .value_name("INTEGER")
                        .value_parser(value_parser!(u8).range(1..))
                        .help("the number of values to generate"),
                ),
        )
        .subcommand(
            Command::new(CHECK_PUB_KEYS)
                .about("validate checksum-hex-encoded public keys")
                .arg(
                    Arg::new(FILE)
                        .value_name("PATH")
                        .required(true)
                        .value_parser(clap::value_parser!(PathBuf))
                        .help("path to input file containing checksum-hex strings (one per line) to be verified"),
                ),
        )
        .subcommand(
            Command::new(CHECK_RANDOM)
                .about("validate checksum-hex-encoded random data")
                .arg(
                    Arg::new(FILE)
                        .value_name("PATH")
                        .required(true)
                        .value_parser(clap::value_parser!(PathBuf))
                        .help("path to input file containing checksum-hex strings (one per line) to be verified"),
                ),
        )
}

fn main() {
    let command_matches = cli().get_matches();
    let (subcommand_name, arg_matches) = command_matches.subcommand().unwrap_or_else(|| {
        let _ = cli().print_long_help();
        println!();
        process::exit(1);
    });

    match subcommand_name {
        GEN_PUB_KEYS => generate_public_keys(arg_matches),
        GEN_RANDOM => generate_random_data(arg_matches),
        CHECK_PUB_KEYS => check(arg_matches, true),
        CHECK_RANDOM => check(arg_matches, false),
        _ => unreachable!(),
    }
}

fn generate_public_keys(arg_matches: &ArgMatches) {
    let count = *arg_matches.get_one::<u8>(COUNT).unwrap();

    for i in 0..count {
        let secret_key = if i < (count + 1) / 2 {
            SecretKey::generate_ed25519()
        } else {
            SecretKey::generate_secp256k1()
        }
        .unwrap();
        let public_key = PublicKey::from(&secret_key);
        let encoded: String = encode_iter(&[public_key.tag()])
            .chain(encode_iter(&Vec::<u8>::from(&public_key)))
            .collect();
        assert_eq!(PublicKey::from_hex(&encoded).unwrap(), public_key);
        println!("{}", encoded);
    }
}

fn generate_random_data(arg_matches: &ArgMatches) {
    let count = *arg_matches.get_one::<u8>(COUNT).unwrap();

    let mut rng = rand::thread_rng();

    for _ in 0..count {
        let length = rng.gen_range(1..=SMALL_BYTES_COUNT);
        let mut input = vec![0; length];
        rng.fill_bytes(&mut input);
        let encoded: String = encode_iter(&input).collect();
        checksummed_hex::decode(&encoded).unwrap();
        println!("{}", encoded);
    }
}

fn check(arg_matches: &ArgMatches, are_public_keys: bool) {
    let file_path = arg_matches.get_one::<PathBuf>(FILE).unwrap();
    let contents = match fs::read_to_string(file_path) {
        Ok(string) => string,
        Err(error) => {
            eprintln!("failed to read {}: {}", file_path.display(), error);
            process::exit(1);
        }
    };

    let mut failed = false;
    for (index, line) in contents.lines().enumerate() {
        if are_public_keys {
            if let Err(error) = PublicKey::from_hex(line) {
                println!("error in line {}: {}", index, error);
                failed = true;
            }
        } else if let Err(error) = checksummed_hex::decode(line) {
            println!("error in line {}: {}", index, error);
            failed = true;
        }
    }

    if failed {
        process::exit(1);
    }

    println!("Passed");
}
